from django.shortcuts import render, redirect
from story7.models import Story7


# Create your views here.
def story7(request):
    data = Story7.objects.all()
    response = {
        'data': data
    }
    return render(request, 'story7.html', response)


def post_data(request):
    if request.method == 'POST':
        data = {
            'name': request.POST['nama'],
            'status': request.POST['status']
        }
        return render(request, 'story7-2.html', data)
    else:
        return redirect('/')


def confirm_status(request):
    if request.method == 'POST':
        data = request.POST
        Story7(nama=data['nama'], pesan=data['status']).save()
        return redirect('/')
    return redirect('/')
