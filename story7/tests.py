import time

from django.http import HttpRequest
from django.test import TestCase, Client, LiveServerTestCase
from story7.models import Story7
from story7.views import story7
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from selenium.webdriver.common.keys import Keys


# Create your tests here.
class Story7TestCase(TestCase):
    def test_models_create(self):
        Story7.objects.create(
            nama='fadiyaaaa',
            pesan='Test'
        )

    def test_landing_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_check_form(self):
        request = HttpRequest()
        response = story7(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)

    def test_check_show_status(self):
        for i in range(5):
            status = Story7.objects.create(nama="fadiya latifah", pesan="lulus sda ppw ddak")
            response = self.client.get('/')
            self.assertEqual(response.status_code, 200)
            self.assertContains(response, status.nama)
            self.assertContains(response, status.pesan)

    def test_check_form_submit_url(self):
        response = self.client.get('/post/')
        self.assertEqual(response.status_code, 302)

    def test_check_form_submit_contains_post_data(self):
        name = "fadiya latifah"
        status = "lulus sda ppw ddak amin"
        response = self.client.post('/post/', data={
            'nama': name,
            'status': status
        })
        self.assertContains(response, name)
        self.assertContains(response, status)


class story7FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--dns-prefetch-disable")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("disable-gpu")
        self.selenium = webdriver.Chrome(
            './chromedriver',
            chrome_options=chrome_options
        )

    def tearDown(self):
        time.sleep(2)
        self.selenium.quit()
        super().tearDown()

    def test_can_post_status_and_appear_on_homepage(self):
        self.selenium.get(self.live_server_url + '/')
        time.sleep(5)
        self.assertIn("Story 7", self.selenium.title)
        form_name = self.selenium.find_element_by_name('nama')
        self.assertEqual(
            form_name.get_attribute('placeholder'),
            "Your Name"
        )
        form_status = self.selenium.find_element_by_name('status')
        self.assertEqual(
            form_status.get_attribute('placeholder'),
            "Your Status"
        )
        form_name.send_keys("Fadiya")
        form_status.send_keys("this is hard")
        submit = self.selenium.find_element_by_class_name('submit-btn')
        submit.send_keys(Keys.RETURN)
        time.sleep(7)
        button_yes = self.selenium.find_element_by_class_name('submit-btn')
        button_yes.click()
        page_text = self.selenium.find_element_by_tag_name('body').text
        self.assertIn("Fadiya", page_text)
        self.assertIn("this is hard", page_text)
