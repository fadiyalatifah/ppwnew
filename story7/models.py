from django.db import models


# Create your models here.
class Story7(models.Model):
    nama = models.CharField(max_length=40)
    pesan = models.TextField()

    def __str__(self):
        return self.nama + " - " + self.pesan
